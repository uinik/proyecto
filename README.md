# Proyecto Git

Proyecto final del curso de git. El objetivo es crear un site sencillo usando Bootstrap, el site tendrá como contenido las bases de git que se cubrieron en el curso y una pequeña biografía o semblanza de cada alumno, La semblanzá incluirá una presentación del alumno, intereses personales y descripción general de lo aprendido.

## Development
- Trello Board: https://trello.com/b/m8RdivpZ
- URL site: https://ugto-git.netlify.com/

## Recursos
- Diapositivas curso: https://www.dropbox.com/sh/u65szab57oxkdvo/AAAQW7TB86rNry1EbVUy_JHUa?dl=0
- Tutorial Git (inglés): https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud
- Bootstrap (inglés): https://getbootstrap.com/docs/4.1/getting-started/introduction/
- Bootstrap - Tipografía (inglés): https://getbootstrap.com/docs/4.1/content/typography/
- Bootstrap - Imágenes (inglés): https://getbootstrap.com/docs/4.1/content/images/

## Consideraciones
- Tener contenido, no diseño.
- Todos los archivos de contenido deben de ir dentro de la carpeta de `includes`, por ejemplo el alumno Jose, tendría su archivo en `includes/jose.html` 
